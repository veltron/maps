<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Map Repository</title>
    <link rel="stylesheet" href="/css/app.css">
</head>

<body>
@include('partials.navigation')
@yield('container')
@yield('scripts')
</body>

</html>