<form class="dropzone" method="POST" id="map-create" action="/maps" enctype="multipart/form-data">
    {{ csrf_field() }}

    <!-- Name Form input -->
    <div class="form-group">
        {{ Form::label('name', 'Name:') }}
        {{ Form::text('name', null, ['class' => 'form-control', 'required' => '']) }}
    </div>

    <!-- Description Form input -->
    <div class="form-group">
        {{ Form::label('description', 'Description:') }}
        {{ Form::textarea('description', null, ['class' => 'form-control', 'required' => '']) }}
    </div>

    <div class="form-group">
        {{ Form::label('author', 'Did you create this map?') }}
        {{ Form::checkbox('author') }}
    </div>

    <div class="dropzone-previews"></div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Add</button>
    </div>
</form> 