@extends('layout')

@section('container')
    <h1>All Maps</h1>

    <ul class="list-group">
        @foreach($maps as $map)
            <li class=list-group-item>{{ $map->name }}</li>
        @endforeach

    </ul>

@stop