@extends('layout')

@section('container')

    <h1>Add a New Map</h1>

    @include('maps.forms.form')

    @include('partials.errors')

@stop

@section('scripts')
    <script src="/js/all.js"></script>
    <script>
        Dropzone.options.mapCreate = {

            autoProcessQueue: false,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 30,
            acceptedFiles: '.zip, .bsp',

            init: function () {
                var mapDropzone = this;

                this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    mapDropzone.processQueue();
                });
            }
        }

    </script>
@stop