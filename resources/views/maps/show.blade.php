@extends('layout')

@section('container')

    <div class="row">
        <div class="col-md-5">
            <h1>{{ $map->name }}</h1>

            <hr>
            <h2>Tags</h2>
            <div class="form-group">
                <a href="/tags/some/tag" class="btn btn-default" role="button">Skirmish</a>
            </div>

            <hr>

            <p>{{ $map->description }}</p>

            <hr>

            <div class="container-fluid">

                <div class="row">
                        <div class="col-md-offset-2 col-md-5">
                            <ul>
                                <li>Size: 12MB</li>
                                <li>Views 13137</li>
                                <li>Uploaded by: roob</li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <a href="/downloads//mapname" class="btn btn-primary" role="button">Download</a>
                        </div>
                </div>
            </div>

        </div>

        <div class="col-md-7">
            <h2>Images</h2>
            <p>Images go here</p>
        </div>
    </div>

@stop