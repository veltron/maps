<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">BG2 Map Repo</a>
        </div>
        <ul class="nav navbar-nav">
            <li ><a href="/">Home</a></li>
            <li><a href="/maps">Browse Maps</a></li>
        </ul>
        <div id="navbar" class="navbar-collapse collapse">


            @if (! $signedIn)
                <div class="navbar-right">
                    <form class="navbar-form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" name="email" placeholder="Email" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" placeholder="Password" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-success">Sign in</button>
                    </form>
                    <!-- <a href="/register" class="btn btn-info">Register</a> -->
                </div>

            @else
                <div class="navbar-right navbar-form">
                    <span class="user-name">Welcome {{ $user->name }}</span>
                    <a href="/logout" class="btn btn-danger">Logout</a>
                </div>

            @endif


    </div>
</nav>