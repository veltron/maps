<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::post('/maps', 'MapsController@store');
Route::get('/maps', 'MapsController@index');
Route::get('/maps/create', 'MapsController@create');
Route::get('/maps/{name}', 'MapsController@show');
Route::get('/maps/{name}/edit', 'MapsController@edit');

Route::auth();
