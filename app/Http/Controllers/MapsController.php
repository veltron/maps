<?php

namespace App\Http\Controllers;

use App\Http\Requests\MapRequest;
use App\Map;
use App\User;
use App\Http\Requests;

class MapsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['create', 'store']]);

        parent::__construct();
    }

    public function show($name)
    {
        $map = Map::where(compact('name'))->firstOrFail();
        $map->addVisit();
        return view('maps.show', compact('map'));
    }

    public function create()
    {
        return view('maps.create');
    }

    public function store(MapRequest $request)
    {
        $map = new Map($request->all());
        $map->upload($request->file('file'));
        $this->user->publish($map);
        
        return redirect()->action('MapsController@show', $request->name);
    }

    public function index()
    {
        $maps = Map::all()->sortByDesc('views');
        return view('maps.index', compact('maps'));
    }
}
