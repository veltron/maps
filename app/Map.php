<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Map extends Model
{
    protected $fillable = [
        'name',
        'description',
        'author'
    ];

    public function photos()
    {
        return $this->hasMany('App\Photo');
    }

    public function ownedBy(User $user)
    {
        return $this->user_id == $user->id;
    }

    public function upload(UploadedFile $file)
    {
        $this->path = $this->baseDir() . $file->getClientOriginalName();
        $file->move($this->baseDir(), $file->getClientOriginalName());
    }

    protected function baseDir()
    {
        return 'files/maps/';
    }

    public function addVisit()
    {
        $this->visits += 1;
        $this->save();
    }
}
